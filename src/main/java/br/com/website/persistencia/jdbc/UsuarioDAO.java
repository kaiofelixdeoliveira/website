package br.com.website.persistencia.jdbc;

import java.beans.Statement;
import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.website.persistencia.entidade.Usuario;

public class UsuarioDAO {

	public Connection con = ConexaoFactory.getConnection();

	/**
	 * Cadastrar um novo usuario no banco de dados.
	 * 
	 * @param usu recebe um objeto com as informa��es de nome,
	 *        login e senha e inseri no banco de dados
	 */
	public void cadastrar(Usuario usu) {
		// TODO Auto-generated method stub

		String sql = "insert into usuario(nome,login,senha) values (?,?,?)";

		try (PreparedStatement preparador = con.prepareStatement(sql)) {

			preparador.setString(1, usu.getNome());
			preparador.setString(2, usu.getLogin());
			preparador.setString(3, usu.getSenha());
			// executando o comando sql
			preparador.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Este metodo � usado para excluir o usuario do banco de dados.
	 * 
	 * @param usu recebe o id do usuario a ser excluido
	 */
	public void excluir(Usuario usu) {
		// TODO Auto-generated method stub

		String sql = "delete from usuario where id=?";

		// parenteses no try fecha a conex�o automaticamente
		try (PreparedStatement preparador = con.prepareStatement(sql)) {

			preparador.setInt(1, usu.getId());
			// executando o comando sql
			preparador.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Este metodo altera as informa��es de um usuario em especifico.
	 * 
	 * @param usu recebe os valores de nome, senha e id do usuario
	 */

	public void alterar(Usuario usu) {
		// TODO Auto-generated method stub

		String sql = "update usuario set nome=?, login=?, senha=? where id=?";

		try (PreparedStatement preparador = con.prepareStatement(sql)) {

			preparador.setString(1, usu.getNome());
			preparador.setString(2, usu.getLogin());
			preparador.setString(3, usu.getSenha());
			preparador.setInt(4, usu.getId());

			// executando o comando sql
			preparador.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Este metodo verifica se o usuario existe ou n�o, se n�o 
	 * existir cria um novo, por�m, se existir altera as informa��es
	 * do usuario ja existente
	 * 
	 * @param usuario pode ser null ou receber o id do usuario
	 */

	public void salvar(Usuario usuario) {

		if (usuario.getId() != null && usuario.getId() > 0) {

			alterar(usuario);
		} else {
			cadastrar(usuario);
		}
	}

	/**
	 * Buscar registros no banco de dados pelo numero do ID do uduario
	 * 
	 * @param id
	 *            � um inteiro que representa o numero do id do usuario
	 * @return retorna um objeto usuario quando encontra ou nulo quando n�o
	 *         encontra
	 */

	public Usuario buscarPorId(Integer id) {

		String sql = "Select * from usuario where id =?";

		try (PreparedStatement preparador = con.prepareStatement(sql)) {

			preparador.setInt(1, id);

			ResultSet resultado = preparador.executeQuery();
			// Posicionando o cursor
			if (resultado.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(resultado.getInt("id"));
				usuario.setNome(resultado.getString("nome"));
				usuario.setLogin(resultado.getString("login"));
				usuario.setSenha(resultado.getString("senha"));

				return usuario;
			}

		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Reliza a busca de todos registros da tabela de usuarios
	 * 
	 * @return Uma lista de objetos Usuario contendo 0 elementos quando tiver
	 *         registros ou n elementos quando tiver resultado
	 */

	public List<Usuario> buscarTodos() {

		String sql = "Select * from usuario order by id";
		List<Usuario> lista = new ArrayList<Usuario>();
		try (PreparedStatement preparador = con.prepareStatement(sql)) {

			ResultSet resultado = preparador.executeQuery();
			// Posicionando o cursor
			while (resultado.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(resultado.getInt("id"));
				usuario.setNome(resultado.getString("nome"));
				usuario.setLogin(resultado.getString("login"));
				usuario.setSenha(resultado.getString("senha"));

				lista.add(usuario);
			}

		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return lista;
	}
	
	/**
	 * Autentica o usuario perante uma tela de login e senha.
	 * 
	 * @param usuConsulta recebe o nome de login e senha do usuario
	 * 
	 * @return retorna o objeto usuario com todas suas informa��es se o usuario existir
	 *         caso contrario retorna nulo
	 */

	public Usuario autenticar(Usuario usuConsulta) {

		String sql = "Select * from usuario where login=? and senha=?";
		
		try (PreparedStatement preparador = con.prepareStatement(sql)) {
			preparador.setString(1, usuConsulta.getLogin());
			preparador.setString(2, usuConsulta.getSenha());

			ResultSet resultado = preparador.executeQuery();

			if (resultado.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(resultado.getInt("id"));
				usuario.setNome(resultado.getString("nome"));
				usuario.setLogin(resultado.getString("login"));
				usuario.setSenha(resultado.getString("senha"));

				return usuario;

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

}
