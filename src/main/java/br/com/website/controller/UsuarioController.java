package br.com.website.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.website.persistencia.entidade.Usuario;
import br.com.website.persistencia.jdbc.UsuarioDAO;

//http://localhost:8080/website/usucontroller.do
@WebServlet("/usucontroller.do")

public class UsuarioController extends HttpServlet {

	public UsuarioController() {
		System.out.println("Construtor");
	}

	@Override
	public void init() throws ServletException {

		System.out.print("init...");
		super.init();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		
		//faz o browser entender que o conteudo de resposta do servlet � html
		resp.setContentType("text/html");
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		String acao = req.getParameter("acao");
		
		if(acao.equals("exc")){
		String id = req.getParameter("id");
		Usuario usu = new Usuario();
		
		if (id != null) {
			usu.setId(Integer.parseInt(id));
		}
		
		
		usuarioDAO.excluir(usu);
		resp.sendRedirect("usucontroller.do?acao=lis");

		
		}else if(acao.equals("lis")){
			
			List<Usuario> lista = usuarioDAO.buscarTodos();
			
			for(Usuario u : lista){
			
				resp.getWriter().println(u+"<br>");
			
				//pega a lista e empacota
				req.setAttribute("lista",lista);
				
				//cria uma instancia para enviar o pacote lista para o listausu.jsp
				RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/listausu.jsp");
			
				//manda o pacote lista para a pagina listausu.jsp
				dispatcher.forward(req, resp);
			}
		}else if(acao.equals("alt")){
			
		String id = req.getParameter("id");	
		UsuarioDAO UsuarioDAO = new UsuarioDAO();
		Usuario usuario = usuarioDAO.buscarPorId(Integer.parseInt(id));	
		req.setAttribute("usu", usuario);
		RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/formusuario.jsp");
		dispatcher.forward(req,resp);
		
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = req.getParameter("id");
		String nome = req.getParameter("nome");
		String login = req.getParameter("login");
		String senha = req.getParameter("senha");

		Usuario usu = new Usuario();

		//se o id n�o for nulo ele inclui na lista de inclus�o
		if (id != null) {
			usu.setId(Integer.parseInt(id));
		}

		usu.setNome(nome);
		usu.setLogin(login);
		usu.setSenha(senha);

		UsuarioDAO usuarioDAO = new UsuarioDAO();
		usuarioDAO.salvar(usu);
		//Redireciona para a lista
		resp.sendRedirect("usucontroller.do?acao=lis");
	}

	@Override
	public void destroy() {
		System.out.println("destroy");
		super.destroy();
	}
}
