package br.com.website.persistencia.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoFactory {

	/**
	 * Classe usada somente para conectar ao banco dados.
	 * 
	 * @return retorna uma conex�o com o banco de dados
	 */
	public static Connection getConnection() {
		// TODO Auto-generated method stub
		try {
			//for�a a execu��o do driver
			Class.forName("org.postgresql.Driver");
			return DriverManager.getConnection("jdbc:postgresql://localhost:5432/website","postgres","kaio3012");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
			
		}
	}

}
