package br.com.website;

import java.util.List;

import br.com.website.persistencia.entidade.Usuario;
import br.com.website.persistencia.jdbc.UsuarioDAO;

public class TestUsuarioDAO {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// testAlterar();
		// testSalvar();
		//testBuscarTodos();
		testAutenticar();
	}

	public static void testExcluir() {
		Usuario usu = new Usuario();
		usu.setId(4);

		// Intanciando metodo excluir
		UsuarioDAO usuDAO = new UsuarioDAO();
		usuDAO.excluir(usu);

		System.out.println("excluido com sucesso");
	}

	public static void testCadastrar() {
		Usuario usu = new Usuario();
		usu.setNome("Jo�ozinho");
		usu.setLogin("jo�o");
		usu.setSenha("123");

		// cadastrando usuario no BD
		UsuarioDAO usuDAO = new UsuarioDAO();
		usuDAO.cadastrar(usu);

		System.out.println("cadastrado com sucesso");
	}

	public static void testAlterar() {
		Usuario usu = new Usuario();
		usu.setId(2);
		usu.setNome("Jo�ozinho");
		usu.setLogin("jo�oddd");
		usu.setSenha("12355");

		// cadastrando usuario no BD
		UsuarioDAO usuDAO = new UsuarioDAO();
		usuDAO.alterar(usu);

		System.out.println("alterado com sucesso");
	}

	public static void testSalvar() {

		Usuario usuario = new Usuario();
		usuario.setId(0);
		usuario.setNome("juca de Souza");
		usuario.setLogin("juca");
		usuario.setSenha("123");

		UsuarioDAO usuDAO = new UsuarioDAO();
		usuDAO.salvar(usuario);
		System.out.println("Salvo com sucesso");
	}

	public static void testBuscarPorId() {

		UsuarioDAO usuarioDAO = new UsuarioDAO();
		Usuario usuario = usuarioDAO.buscarPorId(4);
		System.out.println(usuario);
	}

	public static void testBuscarTodos() {

		UsuarioDAO usuarioDAO = new UsuarioDAO();
		List<Usuario> lista = usuarioDAO.buscarTodos();
		for (Usuario u : lista) {
			System.out.println(u);

		}
	}

	public static void testAutenticar(){
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		
		Usuario usu = new Usuario();
		usu.setLogin("maria");
		usu.setSenha("123");
		Usuario usuRetorno = usuarioDAO.autenticar(usu);
		System.out.println(usuRetorno);
		
	}
	
}
